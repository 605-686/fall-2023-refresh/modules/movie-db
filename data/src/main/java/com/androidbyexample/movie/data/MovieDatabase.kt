package com.androidbyexample.movie.data

import androidx.room.Database
import androidx.room.RoomDatabase

// ##START 050-database
@Database(
    version = 1,
    entities = [
        MovieEntity::class,
        ActorEntity::class,
        RoleEntity::class,
        RatingEntity::class,
    ],
    exportSchema = false
)
abstract class MovieDatabase: RoomDatabase() {
    abstract val dao: MovieDao
}
// ##END
