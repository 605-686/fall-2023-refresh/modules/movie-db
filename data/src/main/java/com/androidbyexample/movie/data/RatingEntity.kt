package com.androidbyexample.movie.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation
import java.util.UUID

// ##START 050-rating-entity
@Entity
data class RatingEntity(
    @PrimaryKey var id: String = UUID.randomUUID().toString(),
    var name: String,
    var description: String,
)
// ##END

// ##START 050-rating-to-movie
// ONE-TO-MANY relationship (Rating -> Movies)
// NOTE: THIS IS NOT AN ENTITY!
data class RatingWithMovies(
    @Embedded
    val rating: RatingEntity,

    @Relation(
        parentColumn = "id",
        entityColumn = "ratingId",
    )
    val movies: List<MovieEntity>
)
// ##END
