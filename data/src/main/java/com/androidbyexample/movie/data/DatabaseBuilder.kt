package com.androidbyexample.movie.data

import android.content.Context
import androidx.room.Room

// ##START 050-db-builder
fun createDao(context: Context) =
    Room.databaseBuilder(
        context,
        MovieDatabase::class.java,
        "MOVIES"
    )
// uncomment the following to see the SQL queries that are run
//        .setQueryCallback(
//            { sqlQuery, bindArgs ->
//                Log.d("!!!SQL", "SQL Query: $sqlQuery SQL Args: $bindArgs")
//            }, Executors.newSingleThreadExecutor()
//        )
        .build()
        .dao

// ##END
