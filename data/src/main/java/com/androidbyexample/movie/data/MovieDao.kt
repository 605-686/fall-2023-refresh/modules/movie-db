package com.androidbyexample.movie.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import kotlinx.coroutines.flow.Flow

// ##START 050-movie-dao
@Dao
abstract class MovieDao {
    @Query("SELECT * FROM RatingEntity")
    abstract fun getRatingsFlow(): Flow<List<RatingEntity>>

    @Query("SELECT * FROM MovieEntity")
    abstract fun getMoviesFlow(): Flow<List<MovieEntity>>

    @Query("SELECT * FROM ActorEntity")
    abstract fun getActorsFlow(): Flow<List<ActorEntity>>

    // ##START 050-grwm
    @Transaction
    @Query("SELECT * FROM RatingEntity WHERE id = :id")
    abstract suspend fun getRatingWithMovies(id: String): RatingWithMovies
    // ##END

    // ##START 050-gawf
    @Transaction
    @Query("SELECT * FROM ActorEntity WHERE id = :id")
    abstract suspend fun getActorWithFilmography(id: String): ActorWithFilmography
    // ##END

    // ##START 050-gmwc
    @Transaction
    @Query("SELECT * FROM MovieEntity WHERE id = :id")
    abstract suspend fun getMovieWithCast(id: String): MovieWithCast
    // ##END

    @Insert
    abstract suspend fun insert(vararg ratings: RatingEntity)
    @Insert
    abstract suspend fun insert(vararg movies: MovieEntity)
    @Insert
    abstract suspend fun insert(vararg actors: ActorEntity)
    @Insert
    abstract suspend fun insert(vararg roles: RoleEntity)

    @Query("DELETE FROM MovieEntity")
    abstract suspend fun clearMovies()
    @Query("DELETE FROM ActorEntity")
    abstract suspend fun clearActors()
    @Query("DELETE FROM RatingEntity")
    abstract suspend fun clearRatings()
    @Query("DELETE FROM RoleEntity")
    abstract suspend fun clearRoles()

    @Transaction
    open suspend fun resetDatabase() {
        clearMovies()
        clearActors()
        clearRoles()
        clearRatings()

        insert(
            RatingEntity(id = "r0", name = "Not Rated", description = "Not yet rated"),
            RatingEntity(id = "r1", name = "G", description = "General Audiences"),
            RatingEntity(id = "r2", name = "PG", description = "Parental Guidance Suggested"),
            RatingEntity(id = "r3", name = "PG-13", description = "Unsuitable for those under 13"),
            RatingEntity(id = "r4", name = "R", description = "Restricted - 17 and older"),
        )

        insert(
            MovieEntity("m1", "The Transporter", "Jason Statham kicks a guy in the face", "r3"),
            MovieEntity("m2", "Transporter 2", "Jason Statham kicks a bunch of guys in the face", "r4"),
            MovieEntity("m3", "Hobbs and Shaw", "Cars, Explosions and Stuff", "r3"),
            MovieEntity("m4", "Jumanji - Welcome to the Jungle", "The Rock smolders", "r3"),
        )
        insert(
            ActorEntity("a1", "Jason Statham"),
            ActorEntity("a2", "The Rock"),
            ActorEntity("a3", "Shu Qi"),
            ActorEntity("a4", "Amber Valletta"),
            ActorEntity("a5", "Kevin Hart"),
        )
        insert(
            RoleEntity("m1", "a1", "Frank Martin", 1),
            RoleEntity("m1", "a3", "Lai", 2),
            RoleEntity("m2", "a1", "Frank Martin", 1),
            RoleEntity("m2", "a4", "Audrey Billings", 2),
            RoleEntity("m3", "a2", "Hobbs", 1),
            RoleEntity("m3", "a1", "Shaw", 2),
            RoleEntity("m4", "a2", "Spencer", 1),
            RoleEntity("m4", "a5", "Fridge", 2),
        )
    }
}
// ##END