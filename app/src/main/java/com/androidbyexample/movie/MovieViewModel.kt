package com.androidbyexample.movie

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.CreationExtras
import com.androidbyexample.movie.repository.ActorDto
import com.androidbyexample.movie.repository.MovieDatabaseRepository
import com.androidbyexample.movie.repository.MovieRepository
import com.androidbyexample.movie.repository.MovieWithCastDto
import com.androidbyexample.movie.screens.MovieList
import com.androidbyexample.movie.screens.Screen

// ##START 030-create-vm
// ##START 070-delegate-to-repo
class MovieViewModel(
    // ##START 070-repo-via-di
    private val repository: MovieRepository,
    // ##END
): ViewModel(), MovieRepository by repository {
// ##END
    // ##START 040-add-stack
    private var screenStack = listOf<Screen>(MovieList)
        set(value) {
            field = value
            // ##START 040-set-screen
            currentScreen = value.lastOrNull()
            // ##END
        }
    // ##END

    // ##START 040-current-screen
    var currentScreen by mutableStateOf<Screen?>(MovieList)
        private set
    // ##END

    // ##START 040-external-stack-support
    fun pushScreen(screen: Screen) {
        screenStack = screenStack + screen
    }
    fun popScreen() {
        screenStack = screenStack.dropLast(1)
    }
    // ##END

    // ##START 080-vm-factory
    companion object {
        val Factory: ViewModelProvider.Factory = object: ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(
                modelClass: Class<T>,
                extras: CreationExtras
            ): T {
                // Get the Application object from extras
                val application = checkNotNull(extras[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY])
                return MovieViewModel(
                    MovieDatabaseRepository.create(application)
                ) as T
            }
        }
    }
    // ##END
}
// ##END
