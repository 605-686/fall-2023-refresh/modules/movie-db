package com.androidbyexample.movie

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.androidbyexample.movie.screens.Ui
import com.androidbyexample.movie.ui.theme.MovieUi1Theme

class MainActivity : ComponentActivity() {
    // ##START 030-connect-vm
    // ##START 080-vm-call
    private val viewModel by viewModels<MovieViewModel> { MovieViewModel.Factory }
    // ##END
    // ##END

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MovieUi1Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    // ##START 040-exit-app
                    Ui(viewModel) {
                        finish()
                    }
                    // ##END
                }
            }
        }
    }
}
