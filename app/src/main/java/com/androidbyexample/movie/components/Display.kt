package com.androidbyexample.movie.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

// ##START 050-display
@Composable
fun Display(
    text: String,
) {
    Text(
        text = text,
        style = MaterialTheme.typography.titleLarge,
        modifier = Modifier
            // ##START 050-two-padding
            .padding(8.dp)
            .padding(start = 16.dp)
            // ##END
            .fillMaxWidth(),
    )
}
// ##END