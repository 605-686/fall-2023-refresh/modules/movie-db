package com.androidbyexample.movie.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.androidbyexample.movie.R
import com.androidbyexample.movie.components.Display
import com.androidbyexample.movie.components.Label
import com.androidbyexample.movie.repository.MovieWithCastDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MovieDisplayUi(
    // ##START 080-use-movie-dto-display
    // ##START 090-change-to-id
    id: String,
    // ##END
    // ##START 090-fetch
    fetchMovie: suspend (String) -> MovieWithCastDto,
    // ##END
    // ##END
) {
    // ##START 090-fetch-movie
    var movieWithCast by remember { mutableStateOf<MovieWithCastDto?>(null) }
    LaunchedEffect(key1 = id) {
        withContext(Dispatchers.IO) {
            movieWithCast = fetchMovie(id)
        }
    }
    // ##END
    // ##START 050-scaffold
    Scaffold(
        // ##START 050-top
        topBar = {
            TopAppBar(
                title = {
                    // ##START 090-title-fallback
                    Text(text = movieWithCast?.movie?.title ?: stringResource(R.string.loading))
                    // ##END
                }
            )
        }
        // ##END
    ) { paddingValues ->
        // ##START 050-movie-display-column
        // ##START 090-no-ui-if-no-movie
        movieWithCast?.let { movieWithCast ->
            Column(
                modifier = Modifier
                    .padding(paddingValues)
                    .verticalScroll(rememberScrollState())
            ) {
                Label(textId = R.string.title)
                Display(text = movieWithCast.movie.title)
                Label(textId = R.string.description)
                Display(text = movieWithCast.movie.description)
                Label(textId = R.string.cast)
                movieWithCast
                    .cast
                    .sortedBy { it.orderInCredits }
                    .forEach { role ->
                        Display(
                            text = stringResource(
                                R.string.cast_entry,
                                role.character,
                                role.actor.name,
                            )
                        )
                }
            }
        }
        // ##END
        // ##END
    }
    // ##END
}
