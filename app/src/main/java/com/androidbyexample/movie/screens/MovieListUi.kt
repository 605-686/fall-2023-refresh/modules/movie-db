package com.androidbyexample.movie.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.androidbyexample.movie.R
import com.androidbyexample.movie.components.Display
import com.androidbyexample.movie.repository.MovieDto

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MovieListUi(
    // ##START 080-use-movie-dto-list
    movies: List<MovieDto>,
    onMovieClicked: (MovieDto) -> Unit,
    // ##END
    // ##START 080-reset-event
    onResetDatabase: () -> Unit,
    // ##END
) {
    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = stringResource(R.string.movies)) },
                // ##START 080-reset-button
                actions = {
                    IconButton(onClick = onResetDatabase) {
                        Icon(
                            imageVector = Icons.Default.Refresh,
                            contentDescription = stringResource(R.string.reset_database)
                        )
                    }
                }
                // ##END
            )
        },
        modifier = Modifier.fillMaxSize()
    ) { paddingValues ->
        Column(
            modifier = Modifier
                .padding(paddingValues)
                .fillMaxSize()
                .verticalScroll(rememberScrollState())
        ) {
            // ##START 050-movie-list
            movies.forEach { movie ->
                Card(
                    elevation = CardDefaults.cardElevation(
                        defaultElevation = 8.dp,
                    ),
                    // ##START 050-onclick
                    onClick = {
                        onMovieClicked(movie)
                    },
                    // ##END
                    modifier = Modifier.padding(8.dp)
                ) {
                    Row(
                        // ##START 050-alignment
                        verticalAlignment = Alignment.CenterVertically,
                        // ##END
                        modifier = Modifier.padding(8.dp)
                    ) {
                        Icon(
                            imageVector = Icons.Default.Star,
                            contentDescription = stringResource(id = R.string.movie)
                        )
                        Display(text = movie.title)
                    }
                }
            }
            // ##END
        }
    }
}
