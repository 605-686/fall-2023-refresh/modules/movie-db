package com.androidbyexample.movie.screens

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.androidbyexample.movie.MovieViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

// ##START 040-starter-ui
@Composable
fun Ui(
    viewModel: MovieViewModel,
    onExit: () -> Unit,
) {
    // ##START 040-back-handler
    BackHandler {
        viewModel.popScreen()
    }
    // ##END

    // ##START 080-coroutine-scope
    val scope = rememberCoroutineScope()
    // ##END

    // ##START 040-screen-switch
    when (val screen = viewModel.currentScreen) {
        null -> onExit()
        is MovieDisplay -> {
            MovieDisplayUi(
                // ##START 090-pass
                id = screen.id,
                fetchMovie = viewModel::getMovieWithCast,
                // ##END
            )
        }
        MovieList -> {
            // ##START 080-collect-movies
            val movies by viewModel.moviesFlow.collectAsStateWithLifecycle(initialValue = emptyList())
            // ##END
            MovieListUi(
                movies = movies,
                onMovieClicked = { movie ->
                    viewModel.pushScreen(MovieDisplay(movie.id))
                },
                // ##START 080-db-reset
                onResetDatabase = {
                    scope.launch(Dispatchers.IO) {
                        viewModel.resetDatabase()
                    }
                }
                // ##END
            )
        }
    }
    // ##END
}
// ##END