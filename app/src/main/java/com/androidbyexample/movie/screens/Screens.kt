package com.androidbyexample.movie.screens

import com.androidbyexample.movie.repository.MovieDto

// ##START 040-create-screen-state
sealed interface Screen

object MovieList: Screen
// ##START 080-movie-display-screen
// ##START 090-movie-display-screen
data class MovieDisplay(val id: String): Screen
// ##END
// ##END
// ##END
