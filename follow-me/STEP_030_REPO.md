---
title: Add Room dependencies
template: main-repo.html
---

Next we add the Room dependencies and the Kotlin Symbol Processor (KSP).

The Kotlin Symbol Processor is a compiler plugin that loads "symbol processors". These processors examine your source code and create new classes as needed, typically looking at annotations in your code. The Room compiler is a symbol processor that generates code based on Room annotations like `@Entity` and `@Database`.

We add the {{ find("030-add-kps-room-versions", "KSP and Room versions") }}, {{ find("030-add-room-deps", "Room compiler and dependencies") }} and {{ find("030-add-ksp", "KSP plugin") }} to our version catalog.

!!! note

    Version compatibility can start to get messy as we add more dependencies and plugins.
    As I'm writing this, the latest version of Kotlin was 1.9.10, which matches the Compose Compiler
    Plugin version 1.5.3 (see [Compose to Kotlin Compatibility Map](https://developer.android.com/jetpack/androidx/releases/compose-kotlin)),
    and the latest version of KSP was 1.9.0-1.0.13. Note the "1.9.0" part of that version...

    The Kotlin compiler's plugin API was not stable as I wrote this. As the compiler plugin API
    evolves, compiler plugins must change the API they use. The version convention of the
    KSP plugin indicates which version of the compiler it works with. In this case, 1.9.0 doesn't
    match Kotlin 1.9.10.

    So my only choice here is to back down my Kotlin version to 1.9.0, which then means I need to
    consult [Compose to Kotlin Compatibility Map](https://developer.android.com/jetpack/androidx/releases/compose-kotlin)
    which requires Compose compiler plugin 1.5.2.

    (I'm working on this exact issue, trying to make it easier to determine the correct set of
    version requirements...)

We want to specify the version of plugins to use in one place, in case they're used in multiple modules. We do this by adding the
plugin to the {{ find("030-specify-ksp-at-root", "top `build.gradle.kts`") }} but don't apply it (so it doesn't actually do anything there other than specify the version to use).

Then we tell the `data` module to {{ find("030-add-ksp-to-data-module", "apply")}} the KSP plugin, and set up the {{ find("030-add-room-data-module", "Room dependencies")}}. The `implementation` dependencies will be available for compilation and included in the built application. The `ksp` dependency will only be used by KSP as a symbol processor to generate code based on the Room annotations.

We're only using Room in the `data` module, so we don't need to add these dependencies in the `repository` module's `build.gradle.kts` file.

Remember after changing the version catalog and gradle build files to re-synchronize by pressing the elephant icon (or the "Sync Now" link at the top of the build files)!

At this point, nothing will visibly change in the application, but it should still build.

## All code changes
