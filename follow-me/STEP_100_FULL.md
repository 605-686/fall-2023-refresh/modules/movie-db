---
title: Viewing the Database
---

You can use the App Inspector in Android Studio to view the database on the emulator or physical device.

1. Run your application
2. Open App Inspection
   * via the "..." on the left edge of Android Studio, or
   * via View -> Tool Windows -> App Inspection from the hamburger menu at the top-left of Android Studio
3. You may need to wait a moment for the running devices dropdown to appear
4. Click on the running devices dropdown
5. Choose the emulator or physical device that's running your application
6. Choose the application that matches the app id in your app/build.gradle.kts
7. Click on the Database Inspector tab
8. You can then double-click table names to see data or run SQL queries

We'll stop here for now and flesh this example out more over time.
We'll fill in all of the details for displaying `Movies`, `Actors` and `Ratings` in a later module, after we've discussed lists.
