---
title: Add new modules
template: main-repo.html
---

!!! note

    You will not need to add any code manually in this step. The displayed diffs are here to show you what gets added when you add the new modules.

We start with the basic Movie UI created in [Initial Movies UI](../movie-ui-1/index.md).

The first thing we need to do is add two modules, `data` and `repository`. You can name these anything you'd like, but I recommend these names as they represent what you're doing pretty well.

To create these modules:

1. Right-click the top-level project in the **Project** view
2. Choose **New -> Module**

   ![Choosing New Module](screenshots/create-data-module-1.png)

3. Select the **Android Library** template

   !!! note

       We choose **Android Library** here because we'll be using Room, which requires Android dependencies such as
       a context to create the database instance. If the module did not have any Android dependencies, we could
       instead create a **Java or Kotlin Library** module.

4. Enter `data` as the **Module name**
5. Update the **Package name** to _lastname_._firstname_._appname_.data

   ![Choosing New Module](screenshots/create-data-module-2.png)

6. Press **Finish**
7. Press **Add** to add the newly-created files to git

   ![Choosing New Module](screenshots/create-data-module-3.png)

Repeat for the `repository` module.

Delete the `libs` directory in each module (`app`, `data`, and `repository`). This is used to host local copies of jars rather than import them from external repositories or other modules in your project.

Note that `settings.gradle.kts` (at the top level of your project) has been updated to add the two new modules. This is how gradle (and Android Studio) know that these are modules it needs to build.

```kotlin
include(":app")
include(":data")
include(":repository")
```

## All code changes
