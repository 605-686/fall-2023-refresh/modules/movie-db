---
title: Movie Database
---

We'll enhance our Movie application with a database to store our movies. For now, it's just read-only, but as we add features to the UI we'll add more functionality to the database to support them.

Source code for examples is at
[https://gitlab.com/605-686/fall-2023-refresh/modules/movie-db](https://gitlab.com/605-686/fall-2023-refresh/modules/movie-db)
