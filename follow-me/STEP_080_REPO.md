---
title: Fix movie list
template: main-repo.html
---

Now we'll fix things so we can display the real movie list and add a "reset database" button to the UI.

Right now, the `MainActivity` has

```kotlin
private val viewModel by viewModels<MovieViewModel>()
```

to create or access an existing view model instance. This doesn't pass in the repository instance
that we now need. To do this, we need to create a **factory** that `viewModels` can use to create
the instance. (Alternatively we could use a dependency-injection framework to create things
for us, but that's out of scope right now.)

That factory needs to obtain an instance of the `MovieDatabaseRepository`. So we'll start there
by {{ find("080-repo-factory", "defining a factory there.") }}.

The `companion object` is a singleton object that can be used by all `MovieDatabaseRepository`
instances, or its parts being called via class-qualified functions such as
`MovieDatabaseRepository.create()`. This create function uses the database builder that we exposed
from the data layer to create a return a `MovieDatabaseRepository` instance.

Back in the view model, we {{ find("080-vm-factory", "create another `companion object`") }},
but this one defines a `ViewModelProvider.Factory` that can be used by the
{{ find("080-vm-call", "`viewModels` call") }} in `MainActivity` when it needs to create an
instance of the `MovieViewModel`.

A little bit of cleanup... We will be using `MovieDto` instead of the `Movie` defined in `app`.
So we delete the `Movie` class from `app` and modify the use of it in
{{ find("080-use-movie-dto-list", "`MovieListUi`") }},
{{ find("080-use-movie-dto-display", "`MovieDisplayUi`") }}, and
{{ find("080-movie-display-screen", "`MovieDisplay`") }}.

To get the list of movies, we'll now need to collect a `Flow` in our UI. Collection is how to observe and get
new values from a `Flow`. 

The `collectAsState()` function is defined by Compose to start a coroutine to collect from a `Flow`
and convert it into Compose `State` so it can be observed as part of a `Snapshot`. The collection
stops if the part of the UI tree that contains it is removed. For example, if we collect in
function `a()` and the current composition no longer calls `a()`, the collection stops.

This is great for flow collection in general, but Android adds an extra concern - lifecycles.
When you switch from an application to the home screen, Android may or may not tell the application
to destroy itself. It's possible for coroutines to keep running, and, in the case of collecting
for display on a UI, it's possible that a non-displayed UI might be updated, which could crash.

To get around this, we have `collectAsStateWithLifecycle()`, which stops the collection
if the UI is not active.

For more details on `collectAsState` vs `collectAsStateWithLifecycle()`, see
[Consuming flows safely in Jetpack Compose](https://medium.com/androiddevelopers/consuming-flows-safely-in-jetpack-compose-cde014d0d5a3).

To use `collectAsStateWithLifecycle()`, we need to add a new dependency to our `app` module.

To do this, add lifecycle-compose to
{{ find("080-add-lifecycle-compose", "the version catalog") }} and to
{{ find("080-add-lifecycle-dep", "`app/build.gradle.kts`") }}.

Now we can add {{ find("080-collect-movies", "the collection code")}}.

To finish things up, let's add a {{ find("080-reset-button", "reset button") }} to our
`MovieList` that calls a passed-in {{ find("", "080-reset-event") }}.

Our `resetDatabase()` function is defined as a `suspend` function, meaning it must be executed in
a coroutine. To launch a coroutine, we need a coroutine scope, so we'll need to
{{ find("080-coroutine-scope", "create that coroutine scope") }} in Ui.

Finally, we {{ find("080-db-reset", "call `resetDatabase()`") }}
in our view model inside a coroutine. 

!!! note

    I restructured the call to `MovieListUI`'s constructor because it now has multiple
    even lambdas. If one lambda feels more important that the others (or the others have
    reasonable defaults), you can keep it at the end for caller to use the lambda-outside-parens
    style. If there's no obvious main action, or you must specify multiple lambdas on every call,
    I recommend you keep *all* of the lambdas inside the parens with parameter names, and *do not*
    use a lambda outside the params.

    (An exception to this advice - any Composable that has a `content` parameter at the end)

When we first run the application, we'll see and empty movie list. There's no data in the database.

![empty movie list](screenshots/empty-movie-list.png){ width=400 }

Pressing the reset button on the tool bar adds data to the database. Because we're using a `Flow`
to get data, Room adds a trigger to watch for database changes, and emits a new list of movies.
Because the UI is collecting from that `Flow`, the list on screen automatically updates:

![movie list with data](screenshots/movie-list-with-data.png){ width=400 }

## All code changes
