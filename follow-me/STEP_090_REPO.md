---
title: Fetch data for movie display
template: main-repo.html
---

`MovieDisplayUi` still works the same way it used to. We wrap the selected `MovieDto` in a
`MovieDisplay` instance and push it on the screen stack. The `Ui` composable gets the current
screen and calls `MovieDisplayUi` passing in the movie.

There are some problems with this approach:

  * If the user jumps to the home screen and later, back into the application, it's possible that 
    Android may have disposed the application stack. You may want to persist the data for the
    current screen. That data may be stale by the time you return.

  * Once on the screen with that data, something else may change the data in the database, and
    we only have a fixed view of the data from the time the list was displayed.

We'll solve the second problem later. For the first problem, we won't persist the data here,
but we'll change what's being passed to just the ID of the data, and fetch the data inside the
screen. Then, if we decide to persist the data as the user exits, we won't have stale data,
as we'll fetch it fresh each time we display it.

To do this, we'll use a controlled-side effect called `LaunchedEffect` in our screen composable.
`LaunchedEffect` launches a coroutine to perform some processing, and that coroutine keeps
running until:

   * it finishes, or
   * its parent composable is no longer part of the UI tree, or
   * its key changes, in which case the current run is canceled and the code in its lambda is
     re-executed

We start by passing a 
{{ find("090-change-to-id", "movie id") }}
instead of a movie itself, and a
{{ find("090-fetch", "fetch function") }}
to allow the composable to fetch the movie when needed.

We add a {{ find("090-fetch-movie", "`Launched Effect`") }} to fetch the movie. On initial
composition, this starts a coroutine to perform the fetch, which will run and return
a `MovieDto`. On recomposition, it will only restart if the id has changed (or the `MovieDisplayUi`
was removed from the UI tree and re-added.) If the user selected a different movie fast enough,
the existing fetch run would be canceled and a few fetch started. 

Because we might not yet have a title, we need to provide a
{{ find("090-title-fallback", "fallback") }}, which we can easily do using our friend the
"elvis operator" `?:` (which if you turn your head 90 degrees to the left and squint looks a little
like Elvis Presley's eyes and hair, thankyouverymuch).

Using the `let` function allows us to easily
{{ find("090-no-ui-if-no-movie", "omit the user interface") }}
while the movie is loading. Note that we've also added the cast information to the display, as it's
now available!

The drawback to this approach is that the screen will likely blink from a blank screen to
one containing the movie data. The main way around this is to perform the data fetch outside the
function. We'll come back to this approach later.

We modify {{ find("090-movie-display-screen", "`MovieDisplay`") }} to take a `String` id instead of
the movie itself, and pass it in the
{{ find("090-pass", "call to `MovieDisplayUi`") }}, along with an event function. We're using
a Kotlin **function reference** here. If the signature of a function matches the required functional
type, we can just pass in an `object::function` specification for it. In this case,

```kotlin
fetchMovie = viewModel::getMovieWithCast
```

is effectively the same as

```kotlin
fetchMovie = { viewModel.getMovieWithCast() }
```

(I say "effectively" because the second example creates an additional function layer to call,
which may be optimized away by the compiler)

## All code changes
