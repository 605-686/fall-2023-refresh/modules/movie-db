---
title: Modifying the view model to use real data
template: main-repo.html
---

First, we need to provide a `MovieRepository` for the view model to use.

We do this via {{ find("070-repo-via-di", "constructor injection") }}, passing it as a parameter
to the constructor. When we create an instance of the view model, we'll need to pass one in.

Now that the view model knows about a repository, we can get data from that repository
by {{ find("070-delegate-to-repo", "delegation") }}. Using 
`MovieViewModel: MovieRepository by repository` like this generates implementation functions
and properties for those defined in `MovieRepository` that call the functions in the passed-in
repository. This is similar to explicitly writing code like

```kotlin
class MovieViewModel(
    private val repository: MovieRepository,
): ViewModel() {
    val moviesFlow = repository.moviesFlow
    suspend fun getMovieWithCast(id: String) =
        repository.getMovieWithCast(id)

    suspend fun insert(actor: ActorDto) {
        repository.insert(actor)
    }
    ...
}
```

If any of the functions need to be more complex, you can override them.

!!! note

    At this point the code will no longer compile, as we've broken the way movies are exposed to the UI.

## All code changes
