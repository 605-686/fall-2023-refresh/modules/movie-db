---
title: Entities, DAO and Database
template: main-repo.html
---

Let's define the Entities, DAO and Database using Room.

Here's our schema

```mermaid
erDiagram
    RATING ||--o{ MOVIE: movies
    MOVIE ||--o{ ROLE : cast
    ROLE }o--|| ACTOR : appearance
```

What does this mean?

* RATING <-> MOVIE is a one-to-many relationship

    * A RATING can be associated with zero or more MOVIEs
    * A MOVIE has exactly one RATING

* MOVIE <-> ACTOR is a many-to-many relationship

    * An ACTOR can appear in zero or more MOVIEs
    * A MOVIE can cast zero or more ACTORs
    * To implement this, we introduce an associative entity ROLE with a one-to-many relationship on each side
        * A MOVIE can cast zero or more ROLEs
        * An ACTOR can appear in zero or more ROLEs
        * A ROLE can appear in exactly one MOVIE
        * A ROLE is played by exactly one ACTOR

Let's look at the attributes of each entity. All entities are represented as Kotlin data classes.

## Naming

There will be a different types of classes used in this application:

  * **Database entities**: the objects that represent rows in a database. We'll use the suffix "Entity" for these.
  * **Data-layer Plain-Old-Kotlin-Objects (POKOs)**: helper objects for retrieving data (or partial data) from entities. For example, a POKO may retrieve a Rating with a list of all Movies taggged with that Rating. We won't use a suffix for these.  
  * **Data transfer objects**: representations of the data from the `data` module exposed by the `repository` module. This hides the fact that the data is coming from a database, as the repository could be returning it from anywhere.  We're creating a abstraction that doesn't leak the underlying implementation details to the user-interface layer. We'll use the suffix "Dto" for these.

## RatingEntity

Represents an MPAA rating (G, PG, PG-13, R, NR)

| Attribute   | Type   | Key |
|-------------|--------|-----|
| id          | String | PK  |
| name        | String |     |
| description | String |     |

We define {{ find("050-rating-entity", "`RatingEntity`") }} and a {{ find("050-rating-to-movie", "POKO") }} to gather a rating and all of its movies at the same time. Note that this POKO represents a ONE-TO-MANY relation. 

## ActorEntity

An actor that can appear in movies

| Attribute | Type   | Key | Comment |
|-----------|--------|-----|---------|
| id        | String | PK  |         |
| name      | String |     |         |

We define {{ find("050-actor-entity", "`ActorEntity`") }} and two {{ find("050-actor-with-filmography", "POKOs") }} to gather an actor and all associated movies at the same time. Note that these POKOs represent a MANY-TO-MANY relation, gathering the association data in the `RoleEntity` by treating it as a ONE-TO-MANY relation **and** a MANY-TO-ONE relation. The `RoleEntity` is the MANY part in the middle.

## MovieEntity

A movie

| Attribute   | Type   | Key |
|-------------|--------|-----|
| id          | String | PK  |
| title       | String |     |
| description | String |     |
| ratingId    | String | FK  |

We define {{ find("050-movie-entity", "`MovieEntity`") }} and two {{ find("050-movie-with-cast", "POKOs") }} to gather a movie and all associated actors at the same time. Note that these POKOs represent a MANY-TO-MANY relation, gathering the association data in the `RoleEntity` by treating it as a ONE-TO-MANY relation **and** a MANY-TO-ONE relation. The `RoleEntity` is the MANY part in the middle.

## RoleEntity

Association entity that casts actors into movies

| Attribute      | Type   | Key |
|----------------|--------|-----|
| movieId        | String | FK  |
| actorId        | String | FK  |
| character      | String |     |
| orderInCredits | int    |     |

We define {{ find("050-role-entity", "`RoleEntity`") }} to hold the MANY-TO-MANY relation data between `MovieEntity` and `ActorEntity`. It holds additional **association data** describing how the movies and actors are related. Gathering the many-to-many data would be simpler if we weren't interested in the association data. However, to access the association data we'll have to use it at the center to two MANY-TO-ONE relations. See `MovieEntity` and `ActorEntity` above for how this works.

## MovieDao

We start with a {{ find("050-movie-dao", "DAO") }} that has basic create and read operations. We'll make it an abstract class so we can define a concrete implementation for `resetDatabase()`.

All basic query functions immediately return a `Flow` that we'll collect inside a coroutine. When the data is fetched, it will emit the result to the `Flow`. If the data changes, the queries will re-execute and emit the new data to the previously-returned `Flow`.

!!! note

    Functions {{ find("050-grwm", "`getRatingWithMovies()`") }},
    {{ find("050-gawf", "`getActorWithFilmography()`") }},
    and {{ find("050-gmwc", "`getMovieWithCast()`") }} are declared as one-shot functions. For now, this is fine, but it will become an issue later when we allow data updates from the UI.

    The problem is that because we're not fetching these data using a `Flow`, there's no way for the database to send us updated data when the data behind the scenes changes. This will lead to stale data on the screen. For example, if we display a movie with its cast, and delete a cast entry, the entry will be removed from the database, but still be visible on the screen unless we do something manual (ewwwww) to update it. Later we'll switch to using `Flow`s for these queries, but for now, I want to demonstrate how you can use a controlled-side effect inside a composable function to fetch data (and demonstrate why this can lead to stale data).

    Functions like these that return POKOs that use `@Relation` will run multiple queries and must be annotated with `@Transaction`.

Later we'll add update and delete functionality. For now, the only updating we'll do is insert initial data.

## MovieDatabase

A very typical {{ find("050-database", "Room database") }} declaration. The database will contain Movie, Actor, Role and Rating entities and expose a MovieDao.

## DatabaseBuilder.kt

This Kotlin file hosts a {{ find("050-db-builder", "single function") }} that we'll use to create an instance of the database. Defining this function here, in the `data` module, avoids the need to make the `repository` module depend on Room just to create the database instance.

Note that if you want to see the SQL queries that are being run, you can uncomment the `setQueryCallback` call and look at the **Logcat** view at the bottom of Android Studio.

## All code changes
