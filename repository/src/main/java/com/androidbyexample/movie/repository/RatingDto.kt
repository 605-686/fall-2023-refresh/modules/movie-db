package com.androidbyexample.movie.repository

import com.androidbyexample.movie.data.RatingEntity
import com.androidbyexample.movie.data.RatingWithMovies

// ##START 060-rating-dto

data class RatingDto(
    val id: String,
    val name: String,
    val description: String,
)
// ##END

internal fun RatingEntity.toDto() =
    RatingDto(id = id, name = name, description = description)
internal fun RatingDto.toEntity() =
    RatingEntity(id = id, name = name, description = description)

data class RatingWithMoviesDto(
    val rating: RatingDto,
    val movies: List<MovieDto>,
)

// only need the toDto(); we don't use this to do database updates
internal fun RatingWithMovies.toDto() =
    RatingWithMoviesDto(
        rating = rating.toDto(),
        movies = movies.map { it.toDto() },
    )
