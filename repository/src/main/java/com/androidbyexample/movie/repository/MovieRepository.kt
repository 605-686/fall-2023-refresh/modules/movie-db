package com.androidbyexample.movie.repository

import kotlinx.coroutines.flow.Flow

// ##START 060-movie-repo
interface MovieRepository {
    val ratingsFlow: Flow<List<RatingDto>>
    val moviesFlow: Flow<List<MovieDto>>
    val actorsFlow: Flow<List<ActorDto>>

    suspend fun getRatingWithMovies(id: String): RatingWithMoviesDto
    suspend fun getMovieWithCast(id: String): MovieWithCastDto
    suspend fun getActorWithFilmography(id: String): ActorWithFilmographyDto

    suspend fun insert(movie: MovieDto)
    suspend fun insert(actor: ActorDto)
    suspend fun insert(rating: RatingDto)

    suspend fun resetDatabase()
}
// ##END